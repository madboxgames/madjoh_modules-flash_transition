define([
	'require',
	'madjoh_modules/controls/controls',
	'madjoh_modules/page_transition/page_transition',
	'madjoh_modules/custom_events/custom_events',
],
function(require, Controls, PageTransition, CustomEvents){
	var FlashTransition = {
		// --- REGEXP ---
		flashCtrlString : 'activeFlasher',
		flashCtrlReg : new RegExp('(\\s|^)'+'activeFlasher'+'(\\s|$)'),

		flash : function(e){
			var source = e.target;
			while(source.className.indexOf('pageFlasher') === -1) source = source.parentNode;
			
			var groupId;
			var pageId;
			if(!source.getAttribute('data-group') || !source.getAttribute('data-page')){
				// console.log('Old flasher', source);
				var id = source.id.split('_');
				groupId = id[id.length-2];
				pageId = id[id.length-1];
			}else{
				groupId = source.getAttribute('data-group');
				pageId = source.getAttribute('data-page');
			}
			
			FlashTransition.flashParam(groupId, pageId);
		},
		flashParam : function(gpeId, pageId){
			var pages = document.querySelectorAll('#flashingGroup_'+gpeId+'>.flashingPage');

			if(pages[pageId].className.indexOf(PageTransition.activeString)===-1){
				PageTransition.startTransition();
				var actualPage = document.querySelector('#flashingGroup_'+gpeId+'>.flashingPage.'+PageTransition.activeString);

				CustomEvents.fireCustomEvent(pages[pageId], 'flashIn');
				pages[pageId].className += ' '+PageTransition.activeString;
				
				actualPage.className = actualPage.className.replace(PageTransition.activeReg,' ');
				CustomEvents.fireCustomEvent(actualPage, 'flashOut');
				PageTransition.endTransition();
			}
		},
		register : function(group){
			var active = group.querySelector('#'+group.id+'>.flashingPage.'+PageTransition.activeString);
			if(!active){
				var pages = group.querySelectorAll('#'+group.id+'>.flashingPage');
				if(pages.length>0) pages[0].className+=' '+PageTransition.activeString;
			}
		}
	};
	// ------------ REGISTER -----------
		element = document.querySelectorAll('.flashingGroup');
		for(var i=0; i<element.length; i++) FlashTransition.register(element[i]);

	// ----------- LISTENERS -----------
		element = document.querySelectorAll('.pageFlasher');
		for(var i=0; i<element.length; i++) element[i].addEventListener(Controls.click, FlashTransition.flash, false);

	return FlashTransition;	
});