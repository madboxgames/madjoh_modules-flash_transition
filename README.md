# flash_transition #

v1.1.5

This modules enables immediate page transition. The commutation is done roughly from one page to another.

## Upgrades since 1.0.0 ##

- The number of groups that can be defined is unlimited.
- The number of pages that can be defined within a group is unlimited.
- The module now triggers custom events.
- The module now supports the PageTransition 1.1.0 classname conventions.

## Getting Started ##

### Define the HTML properly ###

** Define the groups **

Groups must have the class flashingGroup
Group ids must look like : flashingGroup_{{gpeID}}

```html
<div class="flashingGroup" id="flashingGroup_1">
	[...]
</div>
<div class="flashingGroup" id="flashingGroup_2">
	[...]
</div>
```

N.B. : The order of the gpe IDs doesn't matter. There must just not be several groups with the same id.

** Define the pages inside the group **
```html
<div class="flashingGroup" id="flashingGroup_1">
	<div class="Page transitPage flashingPage">Page 1</div>
	<div class="Page transitPage flashingPage">Page 2</div>
	<div class="Page transitPage flashingPage">Page 3</div>
</div>
[...]
<div class="flashingGroup" id="flashingGroup_2">
	<div class="Page transitPage flashingPage">Page 1 of group 2</div>
	<div class="Page transitPage flashingPage">Page 2 of group 2</div>
</div>
```

** Define the active page inside each group **
```html
<div class="flashingGroup" id="flashingGroup_1">
	<div class="Page transitPage flashingPage activePage">Page 1</div>
	<div class="Page transitPage flashingPage">Page 2</div>
	<div class="Page transitPage flashingPage">Page 3</div>
</div>
<div class="flashingGroup" id="flashingGroup_2">
	<div class="Page transitPage flashingPage">Page 1 of group 2</div>
	<div class="Page transitPage flashingPage activePage">Page 2 of group 2</div>
</div>
```

N.B. : If no activePage is defined in a group, the first page of the group will be the active page by default.

** Define the controllers to switch between pages **

Button ids must look like : something_{{gpeID}}_{{pageNB}}

```html
<div class="flashingGroup" id="flashingGroup_1">
	<div class="Page transitPage flashingPage activePage">
		<span class="pageFlasher" id="flasher_1_1">Go to page 2</span>
	</div>
	<div class="Page transitPage flashingPage">
		<span class="pageFlasher" id="flasher_1_2">Go to page 3</span>
	</div>
	<div class="Page transitPage flashingPage">
		<span class="pageFlasher" id="flasher_1_0">Go to page 1</span>
	</div>
</div>
```

### Control the transition via JavaScript ###

This module uses 3 main methods : 

- ** flash(e) ** : Checks the source id to execute flashParam(gpe, page);
- ** flashParam(gpeId, pageId) ** : Switches to the page number *pageId* of the flashingGroup number *gpeId*. (pageId starts at 0).
- ** register(group) ** : Registers the flashingGroup *group*. It defines the active page if none is pre-defined.

### Using Events ###

This module triggers two custom events :

- ** FlashIn ** When the page appears.
- ** FlashOut ** When the page disappears.

## Full working HTML page ##

```html
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="format-detection" content="telephone=no" />
		<meta name="viewport" content="user-scalable=no, initial-scale=1, width=device-width, height=device-height, minimal-ui"/>
		
		<!-- MADJOH STYLES -->
			<link rel="stylesheet" type="text/css" href="MadJohModules/Styling_1.0.0/Styling_1.0.0.css" id="Styling_1.0.0_css"/>
			<link rel="stylesheet" type="text/css" href="MadJohModules/PageTransition_1.1.0/PageTransition_1.1.0.css" id="PageTransition_1.1.0_css"/>
		<!-- /MADJOH STYLES -->

		<!-- APP STYLES -->
			<link rel="stylesheet" type="text/css" href="app/styles/main.css" />
		<!-- /APP STYLES -->

		<title>Welcome</title>
	</head>

	<body>
		<div class="page-container">
			<section id="p_body">
				<div class="flashingGroup" id="flashingGroup_1">
					<div class="Page transitPage flashingPage activePage">
						<span class="pageFlasher" id="flasher_1_1">Go to page 2</span>
					</div>
					<div class="Page transitPage flashingPage">
						<span class="pageFlasher" id="flasher_1_2">Go to page 3</span>
					</div>
					<div class="Page transitPage flashingPage">
						<span class="pageFlasher" id="flasher_1_0">Go to page 1</span>
					</div>
				</div>

				<!-- CACHE PAGE -->
				<div id="cacheBlock"></div>
			</section>
		</div>

		<div id="pub">
			<section>PUB1</section>
			<section>PUB2</section>
			<section>PUB3</section>
		</div>

		<script type="text/javascript" src="MadJohModules/MadJohRequire_1.0.0/MadJohRequire_1.0.0.js"></script>
		<script data-main="app" type="text/javascript" src="lib/require.js"></script>
	</body>
</html>
```